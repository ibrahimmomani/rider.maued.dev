# Rider APP.

This is the main rider app with laravel 5.2

## Installation

### Development Requirements

- PHP: >=7.0
- MySQL: >=5.7
- Laravel 5: https://laravel.com/docs/5.2/installation
- NodeJS: https://nodejs.org/
- Bower: https://bower.io/
- SQLite extention

### Install Essentials

Open `rider.maued.dev` folder *(Optional, run only when you are not inside the project root folder)*

```
cd rider.maued.dev
```

*All these commands should be executed under the root of rider.maued.dev project*

Install composer dependencies
```
composer install
```

Install npm dependencies
```
npm install
```

Install bower dependencies
```
bower install
```

Set-up Laravel, after these commands, please change `.env` file for your own environment settings
```
sudo cp .env.example .env
sudo chmod -R 777 storage
sudo chmod -R 777 bootstrap/cache
php artisan key:generate
php artisan jwt:generate
```

### Database & Seeding

You need to create a Database e.g. `mauedlive` with Encoding `utf8mb4` and Collation `utf8mb4_unicode_ci`.

`MySQL Query:`

```
CREATE DATABASE `mauedlive` DEFAULT CHARACTER SET = `utf8mb4` DEFAULT COLLATE = `utf8mb4_unicode_ci`;
```

Change database config in `.env` file to the match the database that your just created.

After having database configuration setup, you can now do migrations and seeding.

```
php artisan migrate

php artisan db:seed
```

### Ready to go

Until this point, you should be able to visit the home page. 

For example, if you have set the domain: `http://rider.maued.dev/`.

Just visit: http://rider.maued.dev/

It should prompt you for login, use:

```
rider@maued.dev
Abc12345
```

You are free to change the seeding account information from the file: `database/seeds/UsersTableSeeder.php`

You are done. Yeah!

Explore and start to build on top of it.

## Development Process and Flow

### Essential Knowledge

You will need to know, read and understand fowllowing before you can start build on top of these.

- [ ] [Laravel Docs](https://laravel.com/docs/5.2/)
- [ ] [Restful API Best Practise](http://blog.mwaysolutions.com/2014/06/05/10-best-practices-for-better-restful-api/)
- [ ] [dingo/api Guide](https://github.com/dingo/api/wiki)
- [ ] [Repository Pattern](https://bosnadev.com/2015/03/07/using-repository-pattern-in-laravel-5/?utm_source=prettus-l5-repository&utm_medium=readme&utm_campaign=prettus-l5-repository)
- [ ] [Repository Usage](https://github.com/andersao/l5-repository#usage)
- [ ] [Vue.js Guide](https://vuejs.org/guide/)

We recommend use [PHPStorm IDE](https://www.jetbrains.com/phpstorm/) to build and develop your projects.


#### BaseClass

In order to ultise all features provided by App, you should extended the Class from BaseClass (if there has one), e.g. `BasePresenter`, `BaseController`, etc. 

#### Models

All models are located under `app/Models/` folder.

`User` Model under folder `app/Models/Foundation/` is created by default with primary key `user_id`, you should not change this class heavily because it is used almost everywhere within whole project. You can use global function `auth_user()` to access currently logged in user.

Every model should extend `Sto/Models/BaseModel` which has a observer `Sto/Observers/BaseModelObserver` that you can ultilise all model events within the Model, e.g. `onCreating`, `onCreated`, `onUpdating`, `onDeleting`, etc.

When creating new model, you should do it using command to auto generate realted Repository classes.

```
php artisan make:entity Post
```

#### Web Http

All Web related files are located under `app/Http/` folder.

Web Routes are defined in file `app/Http/routes.php`

Web Controllers are defined in folder `app/Http/Controllers/`

#### Restful API

All API related files are located under `app/Api/` folder.

API Routes are defined in file `app/Api/routes.php`

API Controllers are defined in folder `app/Api/Controllers/`

When you create APIs, you need to test them before you can use it. You should test all APIs using Unit Tests provided or create new Unit Tests. Not recommended to test using Browser or Postman, etc. 

#### Repository Pattern

All repository related files are located under `app/` with specific types as parent folders.

Repositories: `app/Repositories/`

Repositories Eloquent: `app/Repositories/Eloquent/`

Repositories Interface: `app/Repositories/Interfaces/`

Repositories Criteria: `app/Repositories/Criteria/`

Presenters: `app/Presenters/`

Transformers: `app/Transformers/`

Validators: `app/Validators/`

#### Resouces

Angulr Styles and Scripts: `resources/assets/angulr/`

Angulr with Blade views: `resources/views/angulr/`

Vuejs: `resources/assets/js/vue/`

Less styles: `resources/assets/less/app.less`

When made changes in scripts, styles, you will need to run the command.

In development, run:
```
npm run-script dev
```

In production, run:
```
npm run-script prod
```

When you changed theme files, run:
```
npm run-script theme
```

#### API and Unit Tests

Unit Tests: `tests/`

API Unit Tests: `tests/Api/`

#### Debug and Clockwork

View all requests and request information from file `storage/clockwork.sqlite`

#### Database

##### We recommend to use migrations for database structure and seeding. 

Directly changing from database or not follow migrations is strongly NOT recommended.

##### Flow of creating database migrations:

1. Create a migration file (Auto generated when use `php artisan make:entity`), under folder `database/migrations/`

2. Add essential columns to migration file:

You are recommended to use `tablename_id` format as primary incremental key, for example, for table `posts`, you need to use `post_id`, and when this become a foreign key, you should keep the same name in other table `post_id`.
```
$table->increments('post_id');
```

The following columns are always required by `BaseModel`:
```
$table->unsignedInteger('user_id')->index();

$table->unsignedInteger('created_by')->nullable();
$table->timestamp('created_at')->nullable();
$table->ipAddress('created_ip')->nullable();
$table->unsignedInteger('updated_by')->nullable();
$table->timestamp('updated_at')->nullable();
$table->ipAddress('updated_ip')->nullable();
```

And remove timestamps():
```
// $table->timestamps();
```

3. Add factory support, under file `database/factories/ModelFactory.php`

4. Create seeding support, under folder `database/seeds/`

##### Refresh Database Migrations and Seeding

When you added or changed to migration files or seedings, or you just simply want to refresh everything in database:

```
php artisan migrate:refresh --seed
```
