@extends('angulr.layout.master')

@section('app')
    <div class="container w-xxl w-auto-xs">
        <a href="#" class="text-center block m-t">
            <img src="/images/01.png" alt="Taxi Maued" title="Taxi Maued">
        </a>
        <div class="m-b-lg">
            @yield('content')
        </div>
        <div class="text-center">
            <p>
                <small class="text-muted">
                    @include('angulr.layout.parts.footer.copyright')
                </small>
            </p>
        </div>
    </div>
@endsection