<!-- navbar header -->
<div class="navbar-header bg-primary">
    <button class="pull-right visible-xs dk" ui-toggle-class="show" target=".navbar-collapse">
        <i class="glyphicon glyphicon-align-justify"></i>
        <span id="badge-badges" class="badge badge-sm up bg-danger pull-right-xs" style="top: -5px; display: none;">0</span>
    </button>
    <!-- brand -->
    <a href="{{url('')}}" class="navbar-brand text-lt">
        <span class="hidden-folded m-l-xs">
            <img src="/images/06.png" style="max-height: 36px" alt=".">
        </span>
    </a>
    <!-- / brand -->
</div>
<!-- / navbar header -->