<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\Sto\Models\Foundation\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'phone_number' => $faker->phoneNumber,
        'remember_token' => str_random(10),
        'country' => $faker->countryCode,
        'timezone' => $faker->timezone,
        'locale' => $faker->locale,
        'status' => 1,
    ];
});
