<?php

use Illuminate\Database\Seeder;
use Sto\Models\Foundation\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::statement("SET foreign_key_checks=0");
        DB::table('users')->truncate();
        DB::statement("SET foreign_key_checks=1");

        User::create([
            'name' => "Ibrahim Momani",
            'email' => 'momani@sto.com.sa',
            'password' => bcrypt('Abc12345'),
            'phone_number' => '+1234567890',
            'remember_token' => str_random(10),
            'country' => 'JO',
            'timezone' => 'Asia/Amman',
            'locale' => 'en',
            'status' => 1,
        ]);

        factory(User::class, 50)->create();
    }
}
