<?php

use Illuminate\Database\Seeder;

class OAuthClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('oauth_clients')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
        DB::table('oauth_clients')->insert([
            'id' => 'StoAndroidRider',
            'secret' => 'WFYBPbkOBv7hTby8vGL2SPOOq2GKYQdSIDGXc001',
            'name' => 'Sto Android App',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);


        DB::table('oauth_clients')->insert([
            'id' => 'StoIosRider',
            'secret' => 'WFYBPbkOBv7hTby8vGL2SPOOq2GKYQdSIDGXc002',
            'name' => 'Sto iOS App',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
