<?php

namespace Sto\Presenters;

use Sto\Transformers\UserTransformer;

/**
 * Class UserPresenter
 *
 * @package namespace Sto\Presenters;
 */
class UserPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new UserTransformer();
    }
}
