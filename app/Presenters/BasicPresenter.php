<?php

namespace Sto\Presenters;

use Sto\Transformers\BasicTransformer;

/**
 * Class BasicPresenter
 *
 * @package namespace Sto\Presenters;
 */
class BasicPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BasicTransformer();
    }
}
