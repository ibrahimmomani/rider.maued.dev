<?php
/**
 * Created for Sto Api Base.
 * 
 */

namespace Sto\Listeners\User;


use Sto\Events\User\UserLoggedInEvent;
use Sto\Events\User\UserRegisteredEvent;
use Sto\Models\BaseModel;

class UserEventListener
{

    public function __construct()
    {
    }


    /**
     * Handle user login events.
     * @param UserLoggedInEvent $event
     */
    public function onUserLogin(UserLoggedInEvent $event)
    {
    }

    /**
     * Handle user registered events.
     * @param UserRegisteredEvent $event
     */
    public function onUserRegistered(UserRegisteredEvent $event)
    {
    }

    /**
     * Handle user logout events.
     */
    public function onUserLogout($event)
    {
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            UserLoggedInEvent::class,
            'Sto\Listeners\User\UserEventListener@onUserLogin'
        );

        $events->listen(
            UserRegisteredEvent::class,
            'Sto\Listeners\User\UserEventListener@onUserRegistered'
        );

//        $events->listen(
//            'Sto\Events\UserLoggedOut',
//            'Sto\Listeners\UserEventListener@onUserLogout'
//        );
    }

}