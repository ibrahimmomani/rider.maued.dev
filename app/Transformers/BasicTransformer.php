<?php
/**
 * Created for Sto Api Base.
 * 
 */

namespace Sto\Transformers;


class BasicTransformer extends BaseTransformer
{

    /**
     * Transform the Any entity
     *
     * @param $value
     * @return mixed
     */
    public function transform($value)
    {
        return $value;
    }
    
}