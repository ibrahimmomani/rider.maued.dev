<?php
/**
 * Created for Sto Api Base.
 * 
 */

namespace Sto\OAuth;


use Illuminate\Support\Facades\Auth;

class PasswordGrantVerifier
{
    public function verify($username, $password)
    {

        $field = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone_number';
        $credentials = [
            $field    => $username,
            'password' => $password,
        ];

        if (Auth::once($credentials)) {
            return Auth::user()->user_id;
        }

        return false;
    }
}