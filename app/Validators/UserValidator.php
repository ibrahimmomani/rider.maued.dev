<?php

namespace Sto\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class UserValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'email' => 'required|email|unique:users',
            'name' => 'required',
            'password' => 'required',
            'phone_number' => 'required|unique:users',
        ],
        ValidatorInterface::RULE_UPDATE => [],
   ];

}